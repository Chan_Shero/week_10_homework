# 第十周作业

#### 介绍
1）.使用Scrapy框架和Selenium配合爬取京东网站商品列表信息（>=50页）： 
网址：https://list.jd.com/list.html?cat=670,671,672 
爬取字段信息由自己定制，这里不做要求。 
2 ). 使用scrapy-redis分布式爬取CSDN学院平台中所有课程信息 
如：https://edu.csdn.net/courses/k 爬取所有课程详情url地址 
然后再通过队列url中对应的每个课程详情信息，使用分布式爬取。 
如：https://edu.csdn.net/course/detail/5466 
要求内容：课程标题，课时、讲师、适合人群、学习人数、价格、课程大纲。 

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)